const SVG_DIMS = {
	dims: {
		padding: {
			top: 10,
			right: 10,
			bottom: 50,
			left: 40
		}
	},
	grid: {
		x: 8,
		y: 8
	}	
}


signalToArr = str => {
	let str_mod = str;
	let i = 0;
	let space_count = 0;
	const data = [];

	str_mod = str_mod.replace('[', '');
	str_mod = str_mod.replace(']', '');
	str_mod = str_mod.replace(';', '');

	str_mod = str_mod.trim();

	const data_strs = str_mod.split(' ');

	for(d of data_strs)
	{
		const tabbed = d.split('\t');

		for(t of tabbed)
		{
			if(t != '')
			{
				data.push(parseFloat(t));
			}
		}
	}

	return data;
}

listToFloat = str => {
	const output = [];

	let data_converted = str.replace('[', '');
	data_converted = data_converted.replace(']', '');

	data_converted = data_converted.split(',');

	for(d of data_converted)
	{
		output.splice(output.length, 0, parseFloat(d));
	}

	return output;
}

getAbsoluteBounds = (input, oldBounds) => {
	let { min, max } = oldBounds;

	for(data of input)
	{
		if(max === undefined || data > max)
		{
			max = data;
		}
		
		if(min === undefined || data < min)
		{
			min = data;
		}
	}

	return {max, min};
}

drawGraph = (svg, absoluteValues) =>
{
	const grid_group = document.createElementNS("http://www.w3.org/2000/svg", "g");
	grid_group.id = 'grid';

	const delta_x = (svg.dims.width - svg.dims.padding.left - svg.dims.padding.right) / svg.grid.x;
	const delta_y = (svg.dims.height - svg.dims.padding.bottom - svg.dims.padding.top) / svg.grid.y; 
	for(let x = 0; x <= svg.grid.x; x++)
	{
		const coords = {
			x: x * delta_x + svg.dims.padding.left
		}

		const line = document.createElementNS("http://www.w3.org/2000/svg", "line");
		line.setAttribute('x1', coords.x);
		line.setAttribute('y1', svg.dims.padding.top - 5);
		line.setAttribute('x2', coords.x);
		line.setAttribute('y2', svg.dims.height - svg.dims.padding.bottom + 5);

		line.classList.add('grid');

		grid_group.append(line);

		const text = document.createElementNS("http://www.w3.org/2000/svg", "text");
		text.setAttribute('x', coords.x + 2);
		text.setAttribute('y', svg.dims.height - svg.dims.padding.bottom + 26);
		text.setAttribute('transform', `rotate(60 ${coords.x} ${svg.dims.height - svg.dims.padding.bottom + 30})`);
		text.style.textAnchor = 'end';
		text.setAttribute('font-size', 10);

		const time = x * delta_x * (absoluteValues.x.max - absoluteValues.x.min) / (svg.dims.width - svg.dims.padding.left - svg.dims.padding.right) + absoluteValues.x.min;
		text.innerHTML = time.toFixed(2);

		grid_group.append(text);
	}

	for(let y = 0; y <= svg.grid.y; y++)
	{
		const coords = {
			y: y * delta_y + svg.dims.padding.top
		}

		const line = document.createElementNS("http://www.w3.org/2000/svg", "line");

		line.setAttribute('x1', svg.dims.padding.left - 5);
		line.setAttribute('y1', coords.y);
		line.setAttribute('x2', svg.dims.width - svg.dims.padding.right + 5);
		line.setAttribute('y2', coords.y);

		line.classList.add('grid');
		grid_group.append(line);

		const text = document.createElementNS("http://www.w3.org/2000/svg", "text");
		text.setAttribute('x', 30);
		text.setAttribute('y', coords.y + 3);
		// text.setAttribute('transform', `rotate(0 ${2} ${coords.y})`);
		text.style.textAnchor = 'end';
		text.setAttribute('font-size', 10);

		const value = absoluteValues.y.max - (y * delta_y * (absoluteValues.y.max - absoluteValues.y.min) / (svg.dims.height - svg.dims.padding.top - svg.dims.padding.bottom));
		text.innerHTML = value.toFixed(2);

		grid_group.append(text);
	}

	svg.ele.append(grid_group);
}

drawSignals = (graph, signals, svg, absoluteValues) =>
{
	// Divide by zero condition for absolute values
	if(absoluteValues.y.max === absoluteValues.y.min)
	{
		absoluteValues.y.max += (absoluteValues.y.max / 5);
		absoluteValues.y.min -= (absoluteValues.y.min / 5);
	}

	if(absoluteValues.x.max === absoluteValues.x.min)
	{
		absoluteValues.x.max += (absoluteValues.x.max / 5);
		absoluteValues.x.min -= (absoluteValues.x.min / 5);
	}

	// Drawing signals
	const group = document.createElementNS("http://www.w3.org/2000/svg", "g");
	group.id = 'data';

	let genCounter = 0;

	for(sig of signals)
	{
		let lastPoint = {};

		const signalGroup = document.createElementNS("http://www.w3.org/2000/svg", "g");
		if(!sig.name)
		{
			sig.name = `gen_${genCounter}`;
			genCounter++;
		}
		signalGroup.id = sig.name;
		

		for(let i = 0; i < sig.x.length; i++)
		{
			const coords = {
				x: ((sig.x[i] - absoluteValues.x.min) * (svg.dims.width - svg.dims.padding.left - svg.dims.padding.right)) / (absoluteValues.x.max - absoluteValues.x.min) + svg.dims.padding.left,
				y: svg.dims.height - (((sig.y[i] - absoluteValues.y.min) * (svg.dims.height - svg.dims.padding.top - svg.dims.padding.bottom)) / (absoluteValues.y.max - absoluteValues.y.min) + svg.dims.padding.bottom)
			}

			if(lastPoint.x !== undefined)
			{
				const line = document.createElementNS("http://www.w3.org/2000/svg", "line");

				line.setAttribute('x1', coords.x);
				line.setAttribute('y1', coords.y);
				line.setAttribute('x2', lastPoint.x);
				line.setAttribute('y2', lastPoint.y);

				if(sig.name)
				{
					line.id = sig.name;
				}

				// Adding style
				if(sig.fill)
				{
					line.style.stroke = sig.fill;
				}

				signalGroup.append(line);
			}

			const point = document.createElementNS("http://www.w3.org/2000/svg", "circle");

			point.setAttribute('cx', coords.x);
			point.setAttribute('cy', coords.y);
			point.setAttribute('r', 2);

			if(sig.name)
			{
				point.id = sig.name;
			}

			// Adding style
			if(sig.fill)
			{
				point.style.fill = sig.fill;
			}

			signalGroup.append(point);

			lastPoint = coords;
		}

		group.append(signalGroup);

		const p_signal = document.createElement('p');
		p_signal.innerHTML = sig.name;
		p_signal.style.color = sig.fill;
		p_signal.classList.add('signal');

		p_signal.dataset.signal = sig.name;

		p_signal.addEventListener('mouseover', e => {
			const signal = e.target.parentElement.querySelector(`svg g#${e.target.dataset.signal}`);
			signal.classList.add('hover');
		})

		p_signal.addEventListener('mouseleave', e => {
			const signal = e.target.parentElement.querySelector(`svg g#${e.target.dataset.signal}`);
			signal.classList.remove('hover');
		})

		const p_signal_popup = document.createElement('p');

		p_signal_popup.innerHTML = `Constant at ${sig.y[0]}`;
		for(let i = 1; i < sig.y.length; i++)
		{
			if(sig.y[0] !== sig.y[i])
			{
				p_signal_popup.innerHTML = '';
				break;
			}
		}
		p_signal_popup.classList.add('popup');

		if(p_signal_popup.innerHTML !== '')
		{			
			p_signal.append(p_signal_popup);
		}
		graph.append(p_signal);
	}

	svg.ele.append(group);
}


const graphs = document.querySelectorAll('.graph:not(.matlab)');

for(graph of graphs)
{
	const svg = {
		ele: graph.querySelector('svg'),
		dims: {
			width: graph.querySelector('svg').getBoundingClientRect().width,
			height: graph.querySelector('svg').getBoundingClientRect().height,
			padding: SVG_DIMS.dims.padding
		},
		grid: SVG_DIMS.grid
	};

	const dataPoints = graph.querySelectorAll('.data');

	const signals = [];
	const absoluteValues = {
		x: {
			max: undefined,
			min: undefined
		},
		y: {
			max: undefined,
			min: undefined
		}
	}


	// Adding buttons to the graph controls
	// if(!graph.classList.contains('single-signal'))
	// {
	// 	const clearBtn = document.createElement('button');
	// 	clearBtn.classList.add('clear');
	// 	clearBtn.innerHTML = 'Clear';

	// 	clearBtn.addEventListener('click', e => {
	// 		const svg = e.target.parentElement.querySelector('svg g#data');
	// 		svg.innerHTML = '';
	// 	})

	// 	graph.append(clearBtn);
	// }

	const signals_container = document.createElement('div');
	signals_container.classList.add('signals');
	graph.append(signals_container);
	
	for(dat of dataPoints)
	{
		const data = {};
		const rawData_arr = dat.innerHTML.split('\n');
		
		// processing data
		for(rawData of rawData_arr)
		{
			if(rawData.trim() !== '' && rawData.trim().indexOf('signal_description') === -1)
			{
				const values = rawData.split('=');

				if(values[0].trim() === 'x')
				{
					data.x = listToFloat(values[1]);
					absoluteValues.x = getAbsoluteBounds(data.x, absoluteValues.x);
				}
				else if(values[0].trim() === 'y')
				{
					data.y = listToFloat(values[1]);
					absoluteValues.y = getAbsoluteBounds(data.y, absoluteValues.y);
				}
				else if(values[0].trim() === 's')
				{
					data.name = values[1].split("'")[1];
				}
				else if(values[0].trim() === 'time{s,g}')
				{
					data.x = signalToArr(values[1]);
					absoluteValues.x = getAbsoluteBounds(data.x, absoluteValues.x);
				}
				else if(values[0].trim() === 'value{s,g}')
				{
					data.y = signalToArr(values[1]);
					absoluteValues.y = getAbsoluteBounds(data.y, absoluteValues.y);
				}
				else if(values[0].trim() === 'gridx')
				{
					svg.grid.x = parseInt(values[1].trim())
				}
				else if(values[0].trim() === 'gridy')
				{
					svg.grid.y = parseInt(values[1].trim())
				}
				else if(values[0].trim() === 'name')
				{
					data.name = values[1].replace(' ', '');
				}
				else
				{
					data[values[0].trim()] = values[1].trim();
				}
			}
		}

		signals.splice(signals.length, 0, data);
	}

	drawGraph(svg, absoluteValues);
	drawSignals(graph, signals, svg, absoluteValues);
}